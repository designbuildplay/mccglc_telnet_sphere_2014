module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      concat: {
                options: {
                  // define a string to put between each file in the concatenated output
                  separator: ';'
                },
                dist: {
                  // the files to concatenate
                  src: ['static/js/src/lib/*.js','static/js/src/*.js'],
                  // the location of the resulting JS file
                  dest: 'static/js/dist/<%= pkg.name %>.js'
                }
              },

      uglify: {
                options: {
                           banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                          },
                dist: {
                       files:{ 
                          'static/js/dist/<%= pkg.name %>.min.js':  ['<%= concat.dist.dest %>']  
                      }
                      }
                },

      cssmin: {
                add_banner: {
                  options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
                  },
                  files: {
                    'static/css/dist/<%= pkg.name %>.min.css': ['static/css/src/lib/*.css','static/css/src/*.css']
                  }
                }
              },

      jshint: {
                files: ['gruntfile.js', 'static/js/src/*.js'],
                options: {
                  // options here to override JSHint defaults
                  globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                  },
                  "-W099": true, // allowed mixed tabs and spaces
                  "supernew": true
                }
              },
      
      watch: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint']
              },

      htmlmin: {                                    
                dist: {                                     
                  options: {                                
                    removeComments: true,
                    collapseWhitespace: true
                  },
                  files: {                                   
                    'views/dist/home.html': 'views/src/home.html',   // 'destination': 'source'
                    'views/dist/error/404.html': 'views/src/error/404.html',
                  }
                }
              }
      });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.registerTask('default', ['concat', 'uglify', 'cssmin','htmlmin']);
 
};
