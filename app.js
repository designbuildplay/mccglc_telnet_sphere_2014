"use strict";

//Includes
var express 	= require('express'),
	app 		= express(),
	controller 	= require('./modules/controller'),
	socket,
	players = [];


// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Application // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

app.engine('html', require('hogan-express'));
app.enable('view cache');

//app settings
app.configure(function(){
	
	app.set('view engine', 'html');
	app.set('layout', 'master');
	app.set('views', __dirname + '/views/');
	//setup app vars
	app.set('namespace', '');
	app.set('title', '');
	app.set('http', 3000);
	app.set('baseurl', '10.0.1.5');

	//set up compression and static files
	var oneDay = 0;//86400000;
	app.use(express.compress());
	app.use('/static', express.static(__dirname + '/static', { maxAge: oneDay }));  

	//setup cookies and sessions
	app.use(express.cookieParser());
	app.use(express.session({secret: ']50-$$-><v26}#dbzE0A6I!`ox3h:-g}ia7+WoEj@giD4.yLTF%{2OY+,~sp%4.)'}));

	//POST body parser
	app.use(express.bodyParser());
});

//routing
app.get('/', controller.home);
app.post('/', controller.sendcommand);

//listen for http requests
//app.listen(app.get('http'), app.get('baseurl'));

console.log('Telnet http Listening on port:.. '+app.get('http'));

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// START SOCKET.io // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

var server = app.listen(app.get('http'));
var io = require('socket.io');
var socket = io.listen(server);
// socket.io connection establishment
socket.sockets.on("connection", onSocketConnection);

// // Socket.io EVENTS /////////////////////////////////////////////////

function onSocketConnection(client) {

	players.push(client.id); // push client id to Player total
	var playerID = client.id;

	console.log("hello client ", client.id);
	console.log("playas in the house ", players.length);

	socket.emit('activate', { hello: 'world', id: playerID, players:players.length }); // test message

	// if(players.length == 2){
	// 	socket.emit('monitor', { mode: 'true', players:players.length }); // test message
	// }

	// // Listen for client disconnected
	client.on("disconnect", onClientDisconnect);

	// Listen for message
	client.on("screenUpdate", switchScreen);
};

function switchScreen(data){
	var monitor = data.advert;
	console.log('sever got ad msg', monitor)

	switch(monitor) {
    case 'intro':
        socket.emit('advert', { ad: 'intro' }); 
        break;

    case 'om1':
        socket.emit('advert', { ad: 'om1' });
        break;
    case 'om2':
        socket.emit('advert', { ad: 'om2' });
        break;

    case 'se1':
        socket.emit('advert', { ad: 'se1' });
        break;
    case 'se2':
        socket.emit('advert', { ad: 'se2' });
        break;

    case 'ult1':
        socket.emit('advert', { ad: 'ult1' });
        break;

    case 'ult2':
        socket.emit('advert', { ad: 'ult2' });
        break;
	}
	
}

function onClientDisconnect(client){
	console.log('client died')
	players.length = players.length -1;
	console.log('players left ', players.length)
}
