'use strict';
var telnet 	= require('./telnet'),
	port 	= 23,
	//host 	= 'rainmaker.wunderground.com'; // DEV server [ public telnet server ]
	//host 	= '10.2.0.152'; // live sphere
	host 	= '10.0.1.4'; // live sphere

// HOME -------------------------------------------------------------------------------------------- /
exports.home = function(req,res){

	//set template structure
	var pageData = {
		layout: 'master',
		template: 'frontpage',
		locals: { title:'Novartis'}
	}

	//render template
	render(res,pageData);
}

// POST COMMAND ------------------------------------------------------------------------------------ /


exports.sendcommand = function(req,res){

	//var command = req.body.command;
	var command1 = req.body.command1;
	var command2 = req.body.command2;
	var command3 = req.body.command3;
	var command4 = req.body.command4;
	var command5 = req.body.command5;
	var command6 = req.body.command6;
	var command7 = req.body.command7;

	console.log("command   ", req.body.command7)

	// CHECK THE MESSAGE AND POST TO TELNET
	if(command1 == "SphereLoop_Seebri"){
		console.log("sending 1..")

		sendMessage('RunTimeline("SphereLoop_Seebri", "SphereLoop_Seebri", 100.0, 100.0, 1, 0, 1)')
	}

	else if(command2 == "SphereBreath_Seebri"){
		console.log("sending 2..")

		sendMessage('RunTimeline("SphereBreath_Seebri", "SphereBreath_Seebri", 100.0, 100.0, 0, 1, 1)')
	}

	else if(command3 == "SphereLoop_Onbrez"){
		console.log("sending 3..")

		sendMessage('RunTimeline("SphereLoop_Onbrez", "SphereLoop_Onbrez", 100.0, 100.0, 1, 0, 1)')
	}

	else if(command4  == "SphereBreath_Onbrez"){
		console.log("sending 4..")
		sendMessage('RunTimeline("SphereBreath_Onbrez", "SphereBreath_Onbrez", 100.0, 100.0, 0, 1, 1)')
	}

	else if(command5  == "SphereLoop_Ultibro"){
		console.log("sending 5..")
		sendMessage('RunTimeline("SphereLoop_Ultibro", "SphereLoop_Ultibro", 100.0, 100.0, 1, 0, 1)')
	}

	else if(command6  == "SphereBreath_Ultibro"){
		console.log("sending 6..")
		sendMessage('RunTimeline("SphereBreath_Ultibro", "SphereBreath_Ultibro", 100.0, 100.0, 1, 0, 1)')
	}

	else if(command7  == "SphereLoop_All3"){
		console.log("sending 7..")
		sendMessage('RunTimeline("SphereLoop_All3", "SphereLoop_All3", 100.0, 100.0, 1, 0, 1)')
	}
	else{
		console.log("not sending..")
	}

	res.json({status:200});
	
}

function sendMessage(command){
	//connect to server
	telnet.connect(host,port, function(socket){
		//send command
		telnet.send(socket,command, function(res){
			//response
			console.log(res);
		});
	});

}


// RENDER PAGE ------------------------------------------------------------------------------------ /
function render(res,data){
	res.locals = data.locals;		
	return res.render(data.template, { partials: data.partials, layout:data.layout});
}