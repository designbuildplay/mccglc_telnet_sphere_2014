'use strict';
var net = require('net'),
	events = require('events'),
	util = require('util');

// CONNECT -------------------------------------------------------------------------------------------- /
exports.connect = function(host,port,callback){

	var socket = net.createConnection(port, host);
	
	socket.setEncoding('UTF8');

	socket.on('connect', function(data) {
	    console.log('connected');
	    callback(socket);
	});

	socket.on('data', function(chunk) {
		console.log(chunk)
	});
	
}

exports.send = function(socket,command){
	socket.write(command+'\r\n', function(){
		console.log(command);
	});
}