
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// DESIGN BUILD PLAY :::: FRONTEND APP CONTROLLER :::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


var readTick1 = false,
	readTick2 = false,
	delayTime = 22000, // 22 seconds
	adDelay = 3,
	monitorMode = false,
	player,
	playerID = "",
	socket,
	timerCount,
	section = 0;
	countOn = true;


// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// SOCKET SETUP :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


socketConnection = function(){

	socket = io.connect("ws://10.0.1.5", {port: 3000, transports: ['websocket']});
	
	// Add a connect listener
    socket.on('connect',function(data) {
      console.log('Client has connected to the server!');
    });
    
    // Add a activate listener
    socket.on('activate', function(data) {
        console.log(data);
        console.log('yo playa ', player)
        
        player = data.players;

        if(playerID == ""){
      		playerID = data.id; // set the player ID
     	}
        socketPlayer(data.id)

        //Anwser to server
        socket.emit('screenUpdate', { advert: 'intro' });
       
    });

    socket.on('advert', function(data) {
        console.log("advert changed.. ", data);
        updateMonitor(data)
    });

    // Add a disconnect listener
    socket.on('disconnect',function() {
      console.log('The client has disconnected!');
    });

    //console.log('sock ', socket)
}

// New socket connection
socketPlayer = function(newID) {

	if(playerID != newID){
		monitorMode = true;
		$('#monitor').css('display','block')
		TweenLite.to( $('#mon-splash'), 1, { alpha:1 })
		console.log("monitor mode!")

	}
	else{
		$('#monitor').css('display','none')
	}
}

updateMonitor = function(data){
	screenshow = data.ad;

	if(monitorMode == true){
		TweenLite.set( $('#mon-splash'), { alpha:0 })
		TweenLite.set( $('#mon-se1'), { alpha:0 })
		TweenLite.set( $('#mon-se2'), { alpha:0 })
		TweenLite.set( $('#mon-om1'), { alpha:0 })
		TweenLite.set( $('#mon-om2'), { alpha:0 })
		TweenLite.set( $('#mon-ult1'), { alpha:0 })
		TweenLite.set( $('#mon-ult2'), { alpha:0 })

		switch(screenshow) {
		    case 'intro':
		        TweenLite.to( $('#mon-splash'), 0.6, { alpha:1, delay:0.1 })
		        break;

		    case 'se1':
		        TweenLite.to( $('#mon-se1'), 0.6, { alpha:1, delay:0.1 })
		       break;
		    
		    case 'se2':
		        TweenLite.to( $('#mon-se2'), 0.6, { alpha:1, delay:0.1 })
		       break;

		    case 'om1':
		        TweenLite.to( $('#mon-om1'), 0.6, { alpha:1, delay:0.1 })
		       break;
		    
		    case 'om2':
		        TweenLite.to( $('#mon-om2'), 0.6, { alpha:1, delay:0.1 })
		       break;

		    case 'ult1':
		        TweenLite.to( $('#mon-ult1'), 0.6, { alpha:1, delay:0.1 })
		       break;
		    
		    case 'ult2':
		        TweenLite.to( $('#mon-ult2'), 0.6, { alpha:1, delay:0.1 })
		       break;
		}
	}

	console.log('update monitor called')
}


// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Frontend Features // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

legalintro = function(){
	console.log('show legal')
	$('#legalintro').css("display","block");
	TweenLite.set( $('#legalintro'), { alpha:0 })
	TweenLite.to( $('#legalintro'), 0.7, { alpha:1 })
}

hideLegal = function(){
	switch(section){
		case 1:
			$("#form3").submit();
		break;

		case 2:
			$("#form1").submit();
		break;

		case 3:
			$("#form5").submit();
		break;
	}
	$('#legalintro').css("display","none");
}


seebriClick = function(){
	section = 2;
	legalintro()
	console.log('seebri..')
	$('#splash').css("display","none");

	TweenLite.set( $('#se-ad1'), { alpha:0 })
	TweenLite.to( $('#se-ad1'), 1, { alpha:1, delay:1 })
	$('#se-ad1').css("display","inline");
	$('#on1').css("display","none");

	$("#form1").submit();
	$('#reset2').css("display","inline");

	socket.emit('screenUpdate', { advert: 'se1' });
	console.log('sent mssg..')
}

onbrezClick = function(){
	section = 1;
	legalintro()
	console.log('onbrez...')
	$('#splash').css("display","none");

	TweenLite.set( $('#on-ad1'), { alpha:0 })
	TweenLite.to( $('#on-ad1'), 1, { alpha:1, delay:1  })
	$('#on-ad1').css("display","inline");
	$("#form3").submit();

	socket.emit('screenUpdate', { advert: 'om1' });
}

ultiClick = function(){
	section = 3;
	legalintro()
	console.log('ultibro...')
	$('#splash').css("display","none");

	TweenLite.set( $('#ult-ad1'), { alpha:0 })
	TweenLite.to( $('#ult-ad1'), 1, { alpha:1, delay:1  })
	$('#se1').css("display","none");
	$('#on1').css("display","none");
	$('#ult-ad1').css("display","inline");
	$("#form5").submit();

	socket.emit('screenUpdate', { advert: 'ult1' });
}

hideAds = function(){
	$('#on-ad1').css("display","none");
	$('#ult-ad1').css("display","none");
	$('#se-ad1').css("display","none");
}

onbrezSect = function(sect){

	resetSections()

	switch(sect)
		{

		case 'ad1':
			$('#on-ad1').css("display","inline");
			TweenLite.to( $('#on-ad1'), 0.8, { scale:2, x:-470, y:340 })
			TweenLite.to( $('#on-ad1'), 0.4, { alpha:0, delay:0.4, onComplete:hideAds })
		 	$('#on-ad2').css("display","inline");
		 	$("#form3").submit();
		  break;

		case 'ad2':
		 	$('#on1').css("display","inline");
		 	// TweenLite.to( $('#on-on1'), 0, {delay:adDelay, onComplete:onbrezSect(1) })
		  break;

		case 'ad3':
			if(countOn == false){
				$("#form7").submit();
				resetSections()
				reloadUrl()
			}
		 	
		  break;

		case 1:
		 	$('#on2').css("display","inline");
		 	$('#reset2').css("display","none");
		  break;

		case 2:
		  	$('#on3').css("display","inline");
		  break;

		case 3:
		  	$('#on4').css("display","inline");
		  break;

		case 4:
		  	$('#on5').css("display","inline");
		  break;
		
		case 5:
		  	$('#on6').css("display","inline");
		  break;
		
		case 6:
		  	$('#on7').css("display","inline");
		  	$("#form4").submit();

		  	countOn = true;
		  	timerCount = setTimeout( function(){ 
		  		countOn = false;
		  		console.log('countdown ended')
		  	}, delayTime );
		  break;

		case 7:
			$('#on8').css("display","inline"); // keep oringal on show
				//pauses then moves slides
				// var pauseSlide = setInterval(function(){ moveSlide() }, delayTime );

				// function moveSlide()
				// {
				// 		if(readTick2 == false){
				// 			readTick2 = true
				// 			console.log(readTick2)
				// 				$('#on8').css("display","inline");
		  // 						$('#btnDownON').css("display","inline");
			 //  				clearInterval(pauseSlide)

			 //  			}
				// }
			
		  break;


		case 72:
			$('#on8').css("display","inline");
		  	$('#btnDownON').css("display","inline");
			
		break;


		case 8:
		  	$('#on10').css("display","inline");
		  	$('#btnUpON').css("display","none");
		  	
		  break;
		
		case 82:
		  	$('#on9').css("display","inline");
		  	$('#btnUpON').css("display","inline");
		  	//$("#form4").submit();
		  break;

		case 9:
		  	$('#on10').css("display","inline");
		  break;
		
		case 10:
		  	$('#on11').css("display","inline");
		  break;

		 case 11:
		  	$('#on12').css("display","inline");
		  	socket.emit('screenUpdate', { advert: 'om2' }); //update monitor
		  break;

		case 12:
			TweenLite.set( $('#on-ad2'), { alpha:0 })
			TweenLite.set( $('#on-ad3'), { alpha:0 })
			TweenLite.to( $('#on-ad3'), 1, { alpha:1 })
			$('#on-ad3').css("display","inline");
		  break;


		default:
		 	resetSections()
		 	reloadUrl()
		}

	console.log("SHOW SECT " +(sect+1))

}


seebriSect = function(sect){

	resetSections()
	console.log('sect...', sect)
	switch(sect)
		{
		case 'ad1':
			$('#se-ad1').css("display","inline");
			TweenLite.to( $('#se-ad1'), 0.5, { alpha:0, onComplete:hideAds })
		 	$('#se-ad2').css("display","inline");
		 	$("#form1").submit();
		  break;

		case 'ad2':
		 	$('#se1').css("display","inline");
		  break;

		case 'ad3':
			if(countOn == false){
				$("#form7").submit();
			 	resetSections()
				reloadUrl()
			}
		  break;

		case 1:
		 	$('#se2').css("display","inline");
		  break;

		case 2:
		  	$('#se3').css("display","inline");
		  break;

		case 3:
		  	$('#se4').css("display","inline");
		  	
		  break;

		case 4:
		  	$('#se5').css("display","inline");
		  	
		  break;
		
		case 5:
		  	$('#se6').css("display","inline");
		  	
		  break;
		
		case 6:
		  	$('#se7').css("display","inline");
		  break;

		case 7:
		  	$('#se8').css("display","inline");
		  break;

		case 8:
		  
		  	 $("#form2").submit();
		  	 $('#se9').css("display","inline");

			  	countOn = true;
			  	timerCount = setTimeout( function(){ 
			  		countOn = false;
			  		console.log('countdown ended')
			  	}, delayTime );
		break;
		
		case 9:
				$('#se10').css("display","inline"); // displays current slide again
				//pauses then moves slides
				// var pauseSlide = setInterval(function(){ moveSlide() }, delayTime );

				// function moveSlide()
				// {
				// 		if(readTick1 == false){
				// 			readTick1 = true
				// 			console.log(readTick1)
				// 			$('#se10').css("display","inline");
			 //  				//$('#btnDownSE').css("display","inline");
			 //  				clearInterval(pauseSlide)

			 //  			}
				// }
		  	
		  break;

		case 10:
		  	$('#se11').css("display","inline");
		  break;

		 case 11:
		  	$('#se12').css("display","inline");
		  break;

		case 12:
		  	$('#se13').css("display","inline");
		  	socket.emit('screenUpdate', { advert: 'se2' }); //update monitor
		  break;

		case 13:
			TweenLite.set( $('#se-ad2'), { alpha:0 })
			TweenLite.set( $('#se-ad3'), { alpha:0 })
			TweenLite.to( $('#se-ad3'), 1, { alpha:1 })
		  	$('#se-ad3').css("display","inline");
		break;

		case 15:
		  	resetSections()
			reloadUrl()
		 break;

		default:
		 	resetSections()
		 	reloadUrl()
		}

	console.log("SHOW SECT " +(sect+1))
}

ultibroSect = function(sect){
	resetSections()

	switch(sect)
	{
		case 'ad1':
			$('#ult-ad1').css("display","inline");
			TweenLite.to( $('#ult-ad1'), 0.5, { alpha:0, onComplete:hideAds })
			$('#ult-ad2').css("display","inline");
			$("#form5").submit();
		break;

		case 'ad2':
			$('#ult1').css("display","inline");
		break;

		case 'ad3':
		console.log('HIT TAT')
		$('#ult-ad3').css("display","inline");
			if(countOn == false){
				$("#form7").submit();
				resetSections()
				reloadUrl()
			}
		break;

		case 1:
			$('#ult2').css("display","inline");
			$('#reset2').css("display","none");
		break;

		case 2:
			$('#ult3').css("display","inline");
		break;

		case 3:
			$('#ult4').css("display","inline");
		break;

		case 4:
			$('#ult5').css("display","inline");
		break;

		case 5:
			$('#ult6').css("display","inline");
		break;

		case 6:
			$('#ult7').css("display","inline");
			$("#form6").submit();

				countOn = true;
			  	timerCount = setTimeout( function(){ 
			  		countOn = false;
			  		console.log('countdown ended')
			  	}, delayTime );
		break;

		case 7:
			$('#ult8').css("display","inline");
		break;

		case 8:
			$('#ult9').css("display","inline");
		break;

		case 9:
			$('#ult10').css("display","inline");
		break;

		case 10:
			$('#ult11').css("display","inline");
		break;

		case 11:
			$('#ult12').css("display","inline");
			socket.emit('screenUpdate', { advert: 'ult2' }); //update monitor
		break;

		case 12:
			$('#ult13').css("display","inline");
		break;

		case 13:
			TweenLite.set( $('#ult-ad2'), { alpha:0 })
			TweenLite.set( $('#ult-ad3'), { alpha:0 })
			TweenLite.to( $('#ult-ad3'), 0.3, { alpha:1 })
			$('#ult-ad3').css("display","inline");
		break;
	}
}

resetSections = function(){

	//hide elements ---------------------------
	$('#on-ad1').css("display","none");
	$('#on-ad1').css("display","none");
	$('#on-ad1').css("display","none");
	$('#on1').css("display","none");
	$('#on2').css("display","none");
	$('#on3').css("display","none");
	$('#on4').css("display","none");
	$('#on5').css("display","none");
	$('#on6').css("display","none");
	$('#on7').css("display","none");
	$('#on8').css("display","none");
	$('#on9').css("display","none");
	$('#on10').css("display","none");
	$('#on11').css("display","none");
	$('#on12').css("display","none");

	$('#se-ad1').css("display","none");
	$('#se-ad1').css("display","none");
	$('#se-ad1').css("display","none");
	$('#se1').css("display","none");
	$('#se2').css("display","none");
	$('#se3').css("display","none");
	$('#se4').css("display","none");
	$('#se5').css("display","none");
	$('#se6').css("display","none");
	$('#se7').css("display","none");
	$('#se8').css("display","none");
	$('#se9').css("display","none");
	$('#se10').css("display","none");
	$('#se11').css("display","none");
	$('#se12').css("display","none");
	$('#se13').css("display","none");
	$('#se14').css("display","none");
	$('#se15').css("display","none");

	$('#ult-ad1').css("display","none");
	$('#ult-ad2').css("display","none");
	$('#ult-ad3').css("display","none");
	$('#ult1').css("display","none");
	$('#ult2').css("display","none");
	$('#ult3').css("display","none");
	$('#ult4').css("display","none");
	$('#ult5').css("display","none");
	$('#ult6').css("display","none");
	$('#ult7').css("display","none");
	$('#ult8').css("display","none");
	$('#ult9').css("display","none");
	$('#ult10').css("display","none");
	$('#ult11').css("display","none");
	$('#ult12').css("display","none");
	$('#ult13').css("display","none");

	$('#btnDownSE').css("display","none");
	$('#btnUpSE').css("display","none");
	$('#btnDownSE2').css("display","none");
	$('#btnUpSE2').css("display","none");

	$('#btnDownON').css("display","none");
	$('#btnUpON').css("display","none");

	$('#reset2').css("display","none");
	//showNav()
	//console.log("reset")
}

showMenu = function(){
	resetSections()
	$('#splash').css("display","inline");
	showNav();

	reloadUrl()
}

showNav = function(){
	TweenLite.set( $('#opt1'), { alpha:1 })
	TweenLite.set( $('#opt2'), { alpha:1 })
	TweenLite.set( $('#opt3'), { alpha:1 })
	TweenLite.to( $('#opt1'), 0.6, { alpha:0, delay:0.2 })
	TweenLite.to( $('#opt2'), 0.6, { alpha:0, delay:0.4 })
	TweenLite.to( $('#opt3'), 0.6, { alpha:0, delay:0.5 })
}


reloadUrl = function(){
	var url = window.location.href;    
	window.location.href = url;
}



// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// START THE SHOW // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


$( document ).ready(function() {
 	
 	FastClick.attach(document.body);

	// Stops form posting to new page
	 $('#form1').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form2').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form3').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form4').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form5').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form6').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

     $('#form7').ajaxForm(function() { 
                //alert("something msg1"); 
     }); 

	console.log('init ajax')

	//fade in nav
	showNav()

	//reset spehere
	$("#form7").submit();

	//start the socket 
	socketConnection()
});